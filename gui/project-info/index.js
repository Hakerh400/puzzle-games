'use strict';

const fs = require('fs');
const path = require('path');
const O = require('../js-util');
const pkgInfo = require('../../package.json');

const pkgName = pkgInfo.name;
const name = pkgName.match(/\/(.+)/)[1];
const author = O.cap(pkgName.match(/@([^\/]+)/)[1]);

const projectInfo = {
  author,
  name,
  pkgInfo,
};

module.exports = projectInfo;