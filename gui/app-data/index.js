'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const O = require('../js-util');
const projectInfo = require('../project-info');

const {author} = projectInfo;

const appDataDir = (() => {
  const {env} = process;
  const envAppData = 'APPDATA';
  const envHome = 'HOME';
  
  const checkDir = dir => {
    if(!fs.existsSync(dir))
      return null;
    
    if(!fs.statSync(dir).isDirectory())
      return null;
    
    return dir;
  };
  
  // const configAppData = config.dirs.appData;
  // 
  // if(configAppData !== null)
  //   return checkDir(configAppData);
  
  if(O.has(env, envAppData))
    return checkDir(env[envAppData]);
  
  if(O.has(env, envHome)){
    const homeDir = env[envHome];
    const {platform} = process;
    
    if(platform === 'linux')
      return checkDir(path.join(homeDir, '.local/share'));
    
    if(platform === 'darwin')
      return checkDir(path.join(homeDir, 'Library/Preferences'));
    
    return null
  }
  
  return null;
})();

const authorDir = (() => {
  if(appDataDir === null)
    return null;
  
  const dir = path.join(appDataDir, author);
  
  if(!fs.existsSync(dir))
    fs.mkdirSync(dir);
  
  return dir;
})();

const cache = O.obj();

const getDir = project => {
  assert(authorDir !== null);
  
  if(O.has(cache, project))
    return cache[project];
  
  const dir = path.join(authorDir, project);
  
  if(!fs.existsSync(dir))
    fs.mkdirSync(dir);
  
  cache[project] = dir;
  return dir;
};

const getDirFromCwd = cwd => {
  const project = path.parse(cwd).name;
  
  return getDir(project);
};

module.exports = {
  appDataDir,
  getDir,
  getDirFromCwd,
};