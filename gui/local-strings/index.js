'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const electron = require('electron');
const O = require('../js-util');
const strsObj = require('./strs');

const buildPth = (pth=[]) => {
  const buildPthNew = (pthStr='') => {
    const pthNew = pthStr !== '' ?
      [...pth, ...pthStr.split('.')] : pth;
    
    return buildPth(pthNew);
  };
  
  const toStr = () => {
    let obj = strsObj;
    
    for(const name of pth){
      if(typeof obj !== 'object') return defaultVal(pth);
      if(obj === null) return defaultVal(pth);
      if(!O.has(obj, name)) return defaultVal(pth);
      
      obj = obj[name];
    }
    
    if(typeof obj !== 'string') return defaultVal();
    
    return obj;
  };
  
  buildPthNew[Symbol.toPrimitive] = toStr;
  
  Object.defineProperty(buildPthNew, 's', {
    get(){
      return String(buildPthNew);
    },
  });
  
  return buildPthNew;
};

const defaultVal = pth => {
  return `\`${pth.join('.')}\``;
};

const mainPth = buildPth();

module.exports = Object.assign(mainPth, {
  strs: strsObj,
});