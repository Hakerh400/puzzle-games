'use strict';

const strs = {
  menu: {
    main: {
      title: 'Games',
    },
  },
  generator: {
    generator: 'generator',
    generate: 'Generate',
    step: 'Step',
    skip: 'Skip',
    play: 'Play',
  },
  games: {
    sudoku: {
      name: 'Sudoku',
      params: [
        'Section width',
        'Section height',
      ],
    },
    slitherlink: {
      name: 'Slitherlink',
      params: [
        'Width',
        'Height',
        'Allow four',
      ],
    },
    fillomino: {
      name: 'Fillomino',
      params: [
        'Width',
        'Height',
        'Maximal number',
      ],
    },
    'black-box': {
      name: 'Black Box',
      params: [
        'Width',
        'Height',
        'Minimal number of circles',
        'Maximal number of circles',
      ],
    },
  },
};

module.exports = strs;