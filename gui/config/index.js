'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const O = require('../js-util');
// const appData = require('../app-data');

const cwd = __dirname;
// const appDir = appData.getDirFromCwd(cwd);
const configFile = path.join(cwd,
  '../../../node-projects/config/config');

// if(!fs.existsSync(configFile))
//   O.wfs(configFile, O.sf({}));

const config = require(configFile);

const get = pth => {
  return pth.split('.').reduce((a, b) => {
    if(!(typeof a === 'object')) return null;
    if(a === null) return null;
    if(!O.has(a, b)) return null;
    return a[b];
  }, config);
};

module.exports = {
  ...config,
  get,
};