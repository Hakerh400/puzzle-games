'use strict';

const fs = require('fs');
const path = require('path');
const cp = require("child_process")
const electron = require('electron');
const assert = require('../assert');
const O = require('../js-util');
const LS = require('../local-strings');

const assertUnique = 0
const assertIsomorphFree = 0

const cwd = __dirname
const projs_dir = path.join(cwd, "../../..")
const csp_dir = path.join(projs_dir, "csp")
const csp_exe = path.join(csp_dir, "main")

class Game extends O.EventEmitter{
  disposed = 0;
  params = null;
  clues_num = null;
  clues = null;
  asgns = null
  missingCnt = null;
  wrongCnt = null;
  listeners = [];
  sol = null;
  g = null;
  history = [];
  historyRedo = [];
  batch = new Map();
  showSolvedMsg = 1;
  
  static getId(){ O.virtual('getId'); }
  static getName(){ O.virtual('getName'); }
  static getParamDescs(){ O.virtual('getParamDescs'); }
  static getDefaultParams(){ O.virtual('getDefaultParams'); }
  static getParamsNum(){ return this.getParamDescs().length; }
  
  setParams1(){ O.virtual('setParams1'); }
  getParamsStr(){ O.virtual('getParamsStr'); }
  aels(){ O.virtual('aels'); }
  render1(){ O.virtual('render1'); }
  
  get ctor(){ return this.constructor; }
  
  after_gen(){}
  save(ser){}
  load(ser){}
  
  setCtx(g){
    if(this.disposed) return;
    this.g = g;
  }
  
  setParams(ps, gen=1, aels=1){
    // const {csp} = this;
    
    assert(Array.isArray(ps));
    assert(this.params === null);
    
    if(!this.setParams1(ps)) return 0;
    
    this.params = ps;
    // this.initCsp(gen);
    // this.sanitizeClues();
    
    // csp.lock();
    // csp.on('assign', this.onAssign.bind(this));
    
    if(aels){
      this.ael('keydown', evt => {
        const {code} = evt;
        const flags = O.evtFlags(evt);
        
        if(flags === 0){
          if(code === 'Backquote'){
            this.showSolvedMsg ^= 1;
            this.render();
            return;
          }
          
          return;
        }
        
        if(flags === 4){
          if(code === 'KeyZ'){
            this.undo();
            return;
          }
          
          if(code === 'KeyY'){
            this.redo();
            return;
          }
          
          return;
        }
      });
      
      // this.aels();
    }
    
    return 1;
  }
  
  async generate(pbf1, pbf2){
    const {params} = this

    const name = this.constructor.getId()
    const params_str = `${[params.length, ...params].join(" ")}`
    const inp = [name, params_str].join("\n")
    
    const out = await new Promise((res, rej) => {
      const out_bufs = []
      const err_bufs = []
      const tmp_bufs = []

      const process_percent = buf => {
        assert(buf.length !== 0)

        const p = Number(buf.toString())

        if(p <= 0.5){
          pbf1(p * 2)
          return
        }

        pbf1(1)
        pbf2((p - 0.5) * 2)
      }

      const proc = cp.spawn(csp_exe, {
        cwd: csp_dir,
      })
  
      proc.stdout.on("data", buf => {
        if(tmp_bufs.length !== 0){
          tmp_bufs.push(buf)
          buf = Buffer.concat(tmp_bufs)
          tmp_bufs.length = 0
        }

        while(buf.length !== 0){
          const i = buf.indexOf(O.cc("("))

          if(i === -1){
            out_bufs.push(buf)
            return
          }

          if(i !== 0){
            out_bufs.push(buf.slice(0, i))
            buf = buf.slice(i)
            continue
          }

          const j = buf.indexOf(O.cc(")"))

          if(j !== -1){
            process_percent(buf.slice(1, j))
            buf = buf.slice(j + 1)
            continue
          }

          tmp_bufs.push(buf)
          break
        }
      })

      proc.stderr.on("data", buf => {
        err_bufs.push(buf)
      })

      proc.on("exit", code => {
        const [stdout, stderr] = [out_bufs, err_bufs]
        .map(bufs => Buffer.concat(bufs).toString().trim())
        const out = `${stdout}\n${stderr}`
        
        if(code !== 0 || stderr.length !== 0)
          return rej(out)
        
        if(tmp_bufs.length !== 0)
          return rej(Buffer.concat(tmp_bufs).toString());

        pbf1(1)
        pbf2(1)
        
        res(out)
      })

      proc.stdin.on("error", O.nop)
      proc.stdout.on("error", O.nop)
      proc.stderr.on("error", O.nop)
  
      proc.stdin.end(inp)
    })

    log(O.sanl(out)[0])
    const clues = out.match(/\-?\d+/g).map(a => Number(a)).slice(2)

    this.set_clues(clues);

    /*if(this.disposed) return;
    
    const {csp, clues} = this;
    const f = i => pbf(i);
    
    csp.on('var', f);
    const sol = await csp.solve(clues);
    csp.removeListener('var', f);
    
    if(this.disposed || csp.disposed){
      this.dispose();
      return;
    }
    
    if(assertIsomorphFree){
      const ok = await this.isomorphFree();
      assert(ok);
    }
    
    return this.sol = sol;*/
  }
  
  async removeClues(pbf){
    /*if(this.disposed) return;
    
    const {csp, clues, cluesPriArr, clueBitsArr} = this;
    const {assignments, given} = csp;
    const groupsNum = clues.length;
    const bitsNum = clueBitsArr.length;
    
    const game1 = this.copy(0, 0);
    const csp1 = this.csp1 = game1.csp;
    const clues1 = game1.clues;
    const clueBitsArr1 = game1.clueBitsArr;
    
    assert(clues1.length === groupsNum);
    assert(clueBitsArr1.length === bitsNum);
    
    const bits = clueBitsArr.map(k => {
      return csp.resolveBit(k);
    });
    
    const bvec = bits.map(b => {
      return csp1.bit(b);
    });
    
    csp1.assert(csp1.neq(clueBitsArr1, bvec));
    
    // assert(csp1.simplify());
    
    for(let i = 0; i !== bitsNum; i++)
      csp1.addExtra(clueBitsArr1[i], bits[i]);
    
    if(assertUnique){
      const sol = await csp1.solve();
      
      if(this.disposed || csp1.disposed){
        this.dispose();
        return;
      }
      
      assert(sol === null);
    }
    
    let n = 0;
    
    const order = cluesPriArr.map(clues => {
      const len = clues.length;
      const arr = O.ca(len, i => n + i);
      
      n += len;
      
      return O.shuffle(arr);
    }).flat();
    
    for(let i = 0; i !== groupsNum; i++){
      pbf(i);
      
      const groupIndex = order[i];
      const group = clues[groupIndex];
      const group1 = clues1[groupIndex];
      const len = group.length;
      
      assert(len !== 0);
      assert(group1.length === len);
      
      for(const k of group1)
        csp1.removeExtra(k);
      
      csp1.unsat = 0;
      csp1.sol = null;
      
      const sol = await csp1.solve();
      
      if(this.disposed || csp1.disposed){
        this.dispose();
        return;
      }
      
      if(sol === null){
        csp.hide(group);
        continue;
      }
      
      for(let i = 0; i !== len; i++){
        const k = group[i];
        const bit = csp.resolveBit(k);
        
        csp1.addExtra(group1[i], bit);
        given.add(k);
      }
    }
    
    this.csp1 = null;*/
  }
  
  ael(name, func){
    const f = evt => {
      if(this.g === null) return;
      if(!func(evt)) return;
      this.afterMove();
    };
    
    this.listeners.push([name, f]);
    O.ael(name, f);
  }
  
  afterMove(){
    this.pushBatch();
    this.render();
  }
  
  render(){
    if(this.disposed) return;
    
    const {g} = this;
    
    this.render1();
    
    if(this.showSolvedMsg && this.missingCnt === 0){
      const fnt = 32;
      const offset = 15;
      let s, col;
      
      if(this.wrongCnt === 0){
        s = 'Correct';
        col = '#0f0';
      }else{
        s = 'Incorrect';
        col = '#f00';
      }
      
      g.font(fnt);
      g.textBaseline = 'top';
      g.textAlign = 'left';
      
      const {width} = g.measureText(s);
      
      g.fillStyle = '#888';
      g.beginPath();
      g.rect(0, 0, width + offset * 2, fnt + offset * 2);
      g.fill();
      g.stroke();
      g.fillStyle = col;
      g.fillText(s, offset, offset);
      
      g.textBaseline = 'middle';
      g.textAlign = 'center';
    }
  }
  
  onAssign(k, p, v){
    const {batch} = this;
    if(batch.has(k)) return;
    
    batch.set(k, p);
  }
  
  pushBatch(){
    const {history, batch} = this;
    if(batch.size === 0) return;
    
    history.push(batch);
    this.batch = new Map();
    this.historyRedo.length = 0;
  }
  
  undoOrRedo(type){
    const h1 = this.getHist(type);
    const h2 = this.getHist(type ^ 1);
    if(h1.length === 0) return;
    
    const {csp} = this;
    const batch = h1.pop();
    
    for(const [k, p] of batch)
      this.assign(k, p);
    
    h2.push(this.batch);
    this.batch = new Map();
    
    this.undoOrRedo1(type, h1, h2);
    this.render();
  }
  
  undo(){ this.undoOrRedo(0); }
  redo(){ this.undoOrRedo(1); }
  
  undoOrRedo1(h1, h2){}
  
  getHist(type){
    if(type === 0) return this.history;
    if(type === 1) return this.historyRedo;
    assert.fail();
  }
  
  copy(gen, aels){
    const game = new this.ctor();
    assert(game.setParams(this.params, gen, aels));
    return game;
  }
  
  async isomorphFree(){
    const {csp, clueBits} = this;
    const csp1 = this.copy(1, 0).csp;
    const {b0, b1} = csp1;
    
    const idents = [...csp.allIdents];
    const idents1 = [...csp1.allIdents];
    const identsNum = idents.length;
    
    assert(idents1.length === identsNum);
    
    const arr = [];
    
    for(let i = 0; i !== identsNum; i++){
      const k = idents[i];
      const k1 = idents1[i];
      const bit = csp.resolveBit(k);
      
      if(clueBits.has(k)){
        csp1.assert(csp1.eq(k1, bit ? b1 : b0));
        continue;
      }
      
      arr.push(csp1.eq(k1, bit ? b0 : b1));
    }
    
    csp1.assert(csp1.orv(arr));
    
    const sol = await csp1.solve();
    assert(!csp1.disposed);
    
    // log(sol ? 'sat' : 'unsat');
    
    return sol === null;
  }
  
  close(){
    if(this.disposed) return;
    
    this.dispose();
    this.emit('close');
  }
  
  disposeCspAux(csp){
    if(csp === null) return;
    csp.dispose();
  }
  
  disposeCsp(){
    this.disposeCspAux(this.csp);
    this.disposeCspAux(this.csp1);
  }
  
  dispose(){
    if(this.disposed) return;
    
    this.disposeCsp();
    
    for(const [name, func] of this.listeners)
      O.rel(name, func);
    
    this.g = null;
    this.csp = null;
    this.csp1 = null;
    this.disposed = 1;
  }

  set_clues(clues){
    assert(this.clues === null)

    const clues_num = clues.length

    this.clues_num = clues_num
    this.clues = clues
    this.asgns = O.ca(clues_num, () => null)

    this.missingCnt = clues.reduce(((acc, val) => {
      return acc + (val < 0)
    }), 0)
    this.wrongCnt = 0
  }

  get_clue_raw(clue){
    assert(clue === (clue | 0))
    assert(clue >= 0 && clue < this.clues_num)

    return this.clues[clue]
  }

  is_given(clue){
    const n = this.get_clue_raw(clue)
    return n >= 0
  }

  get_clue_sol(clue){
    const n = this.get_clue_raw(clue)
    return n >= 0 ? n : ~n
  }

  resolve(clue, strict=0){
    const n = this.get_clue_raw(clue)

    if(n >= 0) return n
    if(strict) assert.fail()

    return this.asgns[clue]
  }

  assign(clue, val=null){
    assert(!this.is_given(clue))

    const {clues, asgns} = this
    const val0 = asgns[clue]

    if(val === val0) return;

    const sol = this.get_clue_sol(clue)

    if(val0 !== null && val0 !== sol) this.wrongCnt--
    if(val !== null && val !== sol) this.wrongCnt++
    if(val0 == null) this.missingCnt--
    if(val == null) this.missingCnt++

    asgns[clue] = val

    this.onAssign(clue, val0, val)
  }
}

module.exports = Game;