'use strict';

const fs = require('fs');
const path = require('path');
const electron = require('electron');
const assert = require('../assert');
const O = require('../js-util');
const LS = require('../local-strings');

class Manager{
  constructor(game){
    this.game = game;
  }
}

module.exports = Manager;