'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const electron = require('electron');
const O = require('../js-util');
const LS = require('../local-strings');
const Game = require('./game');
const cols = require('./cols');

// const testGame = null
const testGame = "fillomino"
const test = !!testGame

const cwd = __dirname;
const cssFile = path.join(cwd, 'style.css');
const gameListFile = path.join(cwd, 'game-list.txt');
const gamesDir = path.join(cwd, 'games');

const mainMenuTitleSize = 50;

const gameList = O.sanl(O.rfs(gameListFile, 1).trim());
const gameNames = O.obj();

for(const gameId of gameList)
  gameNames[gameId] = LS(`games.${gameId}.name`);

gameList.sort((a, b) => gameNames[a] < gameNames[b] ? -1 : 1);

let game = null;
let g = null;
let gPrev = null;
let menuDiv = null;
let menuInput = null;
let onMenuInputUpdate = null;
let genDiv = null;
let tested = 0;

let iw, ih;
let iwh, ihh;

const main = async () => {
  addStyle(cssFile);
  
  O.bion(1);
  O.dbgAssert = 1;
  
  // const src = getGameSpec(name);
  // const spec = specPraser.parse(src);
  
  mkMenuDiv();
  aels();
  
  // game = new Sudoku('sudoku');
  // game.setParams([2, 2]);
  // btnGen.click();
  
  checkResize();
};

const mkMenuDiv = () => {
  menuDiv = O.ceDiv(O.body, 'menu');
  menuInput = O.ceInput(menuDiv, 'text', 'game-search');
  // menuInput.focus();
  
  const gamesDiv = O.ceDiv(menuDiv, 'game-list');
  let list = [];
  
  onMenuInputUpdate = () => {
    const s = menuInput.value.trim().toLowerCase();
    const listNew = gameList.filter(a => a.toLowerCase().includes(s));
    
    if(listNew.length === list.length && listNew.every((a, i) => a === list[i]))
      return;
    
    list = listNew;
    gamesDiv.innerText = '';
    
    for(const gameId of listNew){
      const entry = O.ceDiv(gamesDiv, 'game-entry');
      
      const thumb = O.ce(entry, 'img');
      thumb.classList.add('game-thumb');
      thumb.src = (`http://localhost/csp-logic/thumbs/${gameId}.png`);
      
      const caption = O.ce(entry, 'span');
      caption.classList.add('game-caption');
      caption.innerText = gameNames[gameId];
      
      const targets = [thumb, caption];
      
      for(const target of targets){
        O.ael(target, 'click', evt => {
          O.pd(evt);
          openGame(gameId);
        });
      }
    }
  };
  
  const openGame = gameId => {
    hide_elem(menuDiv);
    mkGenDiv(gameId);
  };
  
  const onKeyDown = evt => {
    const {code} = evt;
    const flags = O.evtFlags(evt);
    
    if(flags !== 0) return;
    if(code !== 'Enter') return;
    if(list.length === 0) return;
    
    openGame(list[0]);
  };
  
  O.ael(menuInput, 'input', onMenuInputUpdate);
  O.ael(menuInput, 'keydown', onKeyDown);
  
  onMenuInputUpdate();
  
  if(test) openGame(testGame)
};

const showMenuDiv = () => {
  show_elem(menuDiv);
  menuInput.value = '';
  menuInput.focus();
  onMenuInputUpdate();
};

const mkGenDiv = gameId => {
  const gameCtor = require(`./games/${gameId}`);
  const gameName = gameCtor.getName();
  const paramsNum = gameCtor.getParamsNum();
  const paramDescs = gameCtor.getParamDescs();
  const defaultParams = gameCtor.getDefaultParams();
  const lsGen = LS('generator');
  
  genDiv = O.ceDiv(O.body, 'gen');
  
  // Title
  
  const genTitle = O.ce(genDiv, 'h1', 'title');
  genTitle.innerText = gameName; //`${gameName} ${lsGen('generator')}`;
  
  // Parameters
  
  const psTable = O.ce(genDiv, 'table', 'params');
  const psBody = O.ce(psTable, 'tbody');
  
  const params = paramDescs.map((desc, index) => {
    const paramId = index + 1;
    const row = O.ce(psBody, 'tr', `param param-${paramId}`);
    
    const descCell = O.ce(row, 'td', 'desc');
    descCell.innerText = `${desc}:`;
    
    const input = O.ceInput(row, 'text', 'input');
    input.value = defaultParams[index];
    
    return input;
  });
  
  // Generate button
  
  const btnGen = O.ce(genDiv, 'button', 'btn btn-gen');
  btnGen.innerText = lsGen('generate');

  // Progress bars
  
  const pgTable = O.ce(genDiv, 'table', 'progress');
  
  const pgBody = O.ce(pgTable, 'tbody');
  let stepsNum = 0;
  
  const mkStep = () => {
    const stepId = ++stepsNum;
    const row = O.ce(pgBody, 'tr', `step step-${stepId}`);
    
    const label = O.ce(row, 'td', 'label');
    label.innerText = `${lsGen('step')} ${stepId}:`;
    
    const pbCell = O.ce(row, 'td', 'progress-bar');
    const pb = O.ce(pbCell, 'progress');
    
    pb.value = 0;
    pb.max = 1;
    
    const percent = O.ce(row, 'td', 'percent');
    percent.innerText = '0%';
    
    const cellSkip = O.ce(row, 'td', 'skip');
    const btnSkip = O.ce(cellSkip, 'button', 'btn btn-skip');
    btnSkip.innerText = lsGen('skip');
    btnSkip.disabled = 1;
    
    return [pb, percent];
  };
  
  const mkSteps = num => {
    return O.ca(num, () => mkStep());
  };
  
  const updatePb = (pb, per) => k => {
    pb.value = k;
    per.innerText = O.percent(k, pb.max + 1);
  };
  
  const [[pb1, per1], [pb2, per2]] = mkSteps(2);
  const u1 = updatePb(pb1, per1);
  const u2 = updatePb(pb2, per2);
  
  // Play button
  
  // const btnPlay = O.ce(genDiv, 'button', 'btn btn-play');
  // btnPlay.innerText = lsGen('play');
  // btnPlay.disabled = 1;
  
  // Event listeners
  
  O.ael(btnGen, 'click', () => {
    (async () => {
      btnGen.disabled = 1;
      
      game = new gameCtor();
      
      const ps = params.map(a => a.value);
      if(!game.setParams(ps)) assert.fail();
      
      // const {clues, clueBits} = game;
      
      pb1.max = 1 //clueBits.size;
      pb2.max = 1 //clues.length;
      
      // const sol = await game.generate(u1);
      // assert(sol);
      // assert(sol !== null);
      
      await game.generate(u1, u2);

      // u1(pb1.max);
      // u2(pb1.max);
      
      // await game.removeClues(u2);
      // u2(pb2.max);
      
      // Display
      
      // log(game.clues.map((a, clue) => game.resolve(clue, 0)));
      
      const onClose = () => {
        if(g !== null) g.canvas.remove();
        
        game = null;
        g = null;
        
        show_elem(genDiv);
        btnGen.disabled = 0;
        u1(0);
        u2(0);
      };
      
      if(game.disposed){
        onClose();
      }else{
        hide_elem(genDiv);
        
        game.on('close', onClose);
        
        g = O.ceCanvas(1).g;
        game.setCtx(g);
        game.after_gen();
      }
      
      // g.set('fillStyle', 'black');
      // g.set('lineCap', 'square');
      // g.set('lineJoin', 'miter');
      // g.capMode = 1;
    })()//.catch(log);
  });
  
  // const rs = O.rand(1e9);
  // O.enhanceRNG();
  // O.randSeed(rs);
  // log(rs);
  
  if(test && !tested){
    tested = 1;
    btnGen.click();
  }
};

const aels = () => {
  O.ael('keydown', onKeyDown);
  O.ael('contextmenu', onContextMenu);
  // O.ael('resize', onUpdateCtx);
};

const onKeyDown = evt => {
  const {code} = evt;
  const flags = O.evtFlags(evt);
  
  if(flags === 0){
    if(code === 'Escape'){
      if(game === null){
        if(genDiv === null) return;
        
        genDiv.remove();
        genDiv = null;
        showMenuDiv();
        
        return;
      }
      
      game.close();
      
      return;
    }
    
    return;
  }
};

const onContextMenu = evt => {
  O.pd(evt);
};

const onUpdateCtx = evt => {
  iw = O.iw;
  ih = O.ih;
  iwh = iw / 2;
  ihh = ih / 2;
  
  g.resize(iw, ih);
  game.render();
};

const renderMenu = () => {
  const cs = cols.menu;
  
  g.fillStyle = cs.bg;
  g.fillRect(0, 0, iw, ih);
  
  g.fillStyle = cs.title;
  g.font(mainMenuTitleSize);
  g.fillText(LS('menu.main.title'), iwh, mainMenuTitleSize / 2);
};

const renderGame = () => {
  g.fillStyle = cols.game.bg;
  g.fillRect(0, 0, iw, ih);
};

const getGameSpec = name => {
  const base = `${name}.${gameSpecExt}`;
  const gameFile = path.join(gamesDir, base);
  
  return O.rfs(gameFile, 1);
};

const addStyle = cssFile => {
  const style = O.ce(O.head, 'style');
  style.innerText = O.rfs(cssFile, 1);
};

const checkResize = () => {
  if(g !== null && (gPrev === null || O.iw !== iw || O.ih !== ih))
    onUpdateCtx();
  
  if(g !== gPrev) gPrev = g;
  
  O.raf(checkResize);
};

const hide_elem = e => {
  e.classList.add('hidden');
};

const show_elem = e => {
  e.classList.remove('hidden');
};

main()//.catch(log);