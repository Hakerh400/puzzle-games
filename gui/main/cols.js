'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const electron = require('electron');
const O = require('../js-util');

const cols = {
  menu: {
    bg: 'white',
    title: 'black',
  },
  game: {
    bg: 'darkgray',
  },
};

module.exports = cols;