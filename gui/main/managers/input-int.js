'use strict';

const fs = require('fs');
const path = require('path');
const electron = require('electron');
const assert = require('../../assert');
const O = require('../../js-util');
const LS = require('../../local-strings');
const Game = require('../game');
const Manager = require('../manager');
const util = require('../util');

const {min, max, floor} = Math;

class InputInt extends Manager{
  constructor(game, grid, tileSize, offset, lb, ub){
    super(game);
    
    this.grid = grid;
    this.w = grid.w;
    this.h = grid.h;
    this.tileSize = tileSize;
    this.offset = offset;
    this.lb = lb;
    this.ub = ub;
    this.selected = null;
  }
  
  aels(){
    const {game, csp, lb, ub, grid, w, h, tileSize} = this;
    const wh = w / 2;
    const hh = h / 2;
    
    assert(lb === 1);
    
    game.ael('mousedown', evt => {
      const {button, clientX, clientY} = evt;
      const {iw, ih, iwh, ihh} = O;
      
      if(button === 0){
        const x = floor((clientX - iwh) / tileSize + wh);
        const y = floor((clientY - ihh) / tileSize + hh);
        
        this.selected = grid.has(x, y) ? [x, y] : null;
        return 1;
      }
    });

    game.ael('keydown', evt => {
      const {selected, csp, grid} = this;
      
      const {code} = evt;
      const flags = O.evtFlags(evt);
      if(flags !== 0) return;
      
      const dir = O.evtDir(code);

      if(selected === null){
        if(dir !== null){
          this.selected = [0, 0]
          return 1
        }

        return
      }

      const [x, y] = selected;

      if(code === "KeyX"){
        this.selected = null
        return 1
      }
      
      if(dir !== null){
        const [x1, y1] = O.dirToAdj(dir, x, y);
        if(!grid.has(x1, y1)) return;
        
        this.selected = [x1, y1];
        return 1;
      }
      
      if(this.is_given(x, y)) return;
      
      const tile = grid.get(x, y);
      const v = this.get(x, y);
      
      if(code === 'Delete'){
        this.set(x, y, null);
        return 1;
      }
      
      if(code === 'Backspace'){
        if(v === null) return;
        
        if(v < 10){
          this.set(x, y, null);
          return 1;
        }
        
        this.set(x, y, v / 10 | 0);
        return 1;
      }
      
      const d = O.evtDigit(code);
      if(d === null) return;
      if(d > ub) return;
      
      if(v === null){
        if(d === 0) return;
        
        this.set(x, y, d);
        return 1;
      }
      
      const v1 = v * 10 + d;
      
      if(v1 <= ub){
        this.set(x, y, v1);
        return 1;
      }
      
      if(d === 0){
        this.set(x, y, null);
        return 1;
      }
      
      this.set(x, y, d);
      return 1;
    });
  }
  
  undoOrRedo(){
    this.selected = null;
  }
  
  render(){
    const {game, selected} = this;
    const {g} = game;
    
    if(selected === null) return;
    
    const [x, y] = selected;
    
    g.fillStyle = '#bbb';
    g.fillRect(x, y, 1, 1);
  }
  
  get(x, y){
    const tile = this.grid.get(x, y);
    if(tile === null) return null;
    
    const v = this.game.resolve(tile);
    if(v === null) return null;
    
    return v + this.offset;
  }
  
  set(x, y, v){
    if(v !== null) assert(v >= this.lb && v <= this.ub);
    
    const tile = this.grid.get(x, y);
    this.game.assign(tile, v !== null ? v - this.offset : null);
  }
  
  is_given(x, y){
    return this.game.is_given(this.grid.get(x, y));
  }
}

module.exports = InputInt;