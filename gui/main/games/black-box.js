'use strict';

const fs = require('fs');
const path = require('path');
const electron = require('electron');
const assert = require('../../assert');
const O = require('../../js-util');
const LS = require('../../local-strings');
const Game = require('../game');
const util = require('../util');

const {min, max, floor} = Math;

const gameId = 'black-box';
const lsGame = LS(`games.${gameId}`);

const tileSize = 50;

class BlackBox extends Game{
  static getId(){ return gameId; }
  static getName(){ return lsGame('name'); }
  static getParamDescs(){ return LS.strs.games[gameId].params; }
  static getDefaultParams(){ return ['3', '3', '1', '1']; }

  setParams1(ps){
    if(ps.some(a => !util.isInt(a, 0, 100))) return 0;

    const [w, h, lb, ub] = ps.map(a => Number(a));
    const area = w * h;
    
    if(w === 0 || h === 0) return 0;
    if(lb > ub) return 0;
    if(ub > area) return 0;
    
    this.w = w;
    this.h = h;
    this.ub = ub;
    this.lb = lb;
    
    this.circsNumStr = `${lb}${
      ub !== lb ? `-${ub}` : ''} circle${
      lb === 1 && ub === 1 ? '' : 's'}`;
    
    return 1;
  }
  
  getParamsStr(){
    return `${this.w}x${this.h} (${this.circsNumStr})`;
  }
  
  initCsp(){
    const {csp, w, h, lb, ub} = this;
    const w2 = w + 2;
    const h2 = h + 2;
    const area = w * h;
    const s = util.bitsNum(area);
    const s1 = util.bitsNum(area + 1);
    const borderIdMax = w + h << 1;
    const sb = util.bitsNum(borderIdMax + 1);
    
    let circsNum = csp.int(0, s1);
    let curBorderId = csp.int(1, sb);
    
    const grid = this.grid = new O.Grid(w2, h2, (x, y) => {
      const hBorder = y === 0 || y === h + 1;
      const vBorder = x === 0 || x === w + 1;
      const border = hBorder || vBorder;
      
      if(hBorder && vBorder) return null;
      
      if(border){
        const id = this.addClue(csp.varLe(borderIdMax), {pri: 1, optional: 1});
        const nhit = csp.orv(id);
        const hit = csp.not(nhit);
        
        csp.assert(csp.le(id, curBorderId));
        curBorderId = csp.inc(curBorderId, csp.eq(id, curBorderId));
        
        return {border: 1, id, hit};
      }
      
      const circ = this.mkVar({pri: 0});
      
      circsNum = csp.inc(circsNum, circ);
      
      return {border: 0, circ};
    });
    
    csp.assert(csp.le(csp.int(lb, s1), circsNum));
    csp.assert(csp.le(circsNum, csp.int(ub, s1)));
  }
  
  aels(){
    const {csp, w, h, grid} = this;
  }
  
  render1(){
    const {csp, g, w, h, lb, ub, grid} = this;
    const {iw, ih, iwh, ihh} = O;
    const w2 = w + 2;
    const h2 = h + 2;
    
    csp.revealHidden();
    
    g.font(32);
    g.lineCap = 'square';
    g.lineJoin = 'miter';
    
    g.fillStyle = 'darkgray';
    g.fillRect(0, 0, iw, ih);
    
    g.translate(iwh, ihh);
    g.scale(tileSize);
    g.translate(-w2 / 2, -h2 / 2);
    
    grid.iter((x, y, d) => {
      if(d === null) return;
      
      g.save();
      g.translate(x, y);
      
      if(d.border){
        g.fillStyle = '#c8bfe7';
        g.fillRect(0, 0, 1, 1);
        
        const id = csp.resolve(d.id);
        
        if(id !== null){
          g.fillStyle = 'black';
          g.fillText(id === 0 ? 'H' : id, .5, .5);
        }
      }else{
        const circ = csp.resolveBit(d.circ);
        
        if(circ === null){
          g.fillStyle = '#6a6a6a';
          g.fillRect(0, 0, 1, 1);
        }else{
          g.fillStyle = 'white';
          g.fillRect(0, 0, 1, 1);
          
          if(circ){
            g.fillStyle = 'black';
            g.beginPath();
            O.drawCirc(g, .5, .5, .4);
            g.fill();
          }
        }
      }
      
      g.restore();
    });
    
    grid.iter((x, y, d) => {
      if(d === null) return;
      
      g.beginPath();
      g.rect(x, y, 1, 1);
      g.stroke();
    });
    
    g.resetTransform();
  }
}

module.exports = BlackBox;