'use strict';

const fs = require('fs');
const path = require('path');
const electron = require('electron');
const assert = require('../../assert');
const O = require('../../js-util');
const LS = require('../../local-strings');
const Game = require('../game');
const util = require('../util');

const {min, max, floor} = Math;

const gameId = 'slitherlink';
const lsGame = LS(`games.${gameId}`);

const tileSize = 50;

class Slitherlink extends Game{
  static getId(){ return gameId; }
  static getName(){ return lsGame('name'); }
  static getParamDescs(){ return LS.strs.games[gameId].params; }
  static getDefaultParams(){ return ['5', '5', 'false']; }

  setParams1(ps){
    if(ps.slice(0, 2).some(a => !util.isInt(a, 1, 100))) return 0;
    
    const allow4 = util.parseProp(ps[2]);
    if(allow4 === null) return 0;
    
    const w = Number(ps[0]);
    const h = Number(ps[1]);
    
    if(w === 1 && h === 1 && !allow4) return 0;
    
    this.w = w;
    this.h = h;
    this.allow4 = allow4;
    
    return 1;
  }
  
  getParamsStr(){
    return `${this.w}x${this.h}${this.allow4 ? '' : ' (no four)'}}`;
  }
  
  initCsp(gen){
    const {csp, w, h, allow4} = this;
    const w1 = w + 1;
    const h1 = h + 1;
    
    const grid = this.grid = new O.Grid(w, h, () => this.addClue(csp.varLt(5), {pri: 1, optional: 1}));
    const hlines = this.hlines = new O.Grid(w, h1, () => this.mkVar({pri: 0}));
    const vlines = this.vlines = new O.Grid(w1, h, () => this.mkVar({pri: 0}));
    
    const n0 = csp.int(0, 3);
    const n2 = csp.int(2, 3);
    
    for(let y = 0; y !== h1; y++){
      for(let x = 0; x !== w1; x++){
        const lines = [
          vlines.get(x, y - 1),
          hlines.get(x, y),
          vlines.get(x, y),
          hlines.get(x - 1, y),
        ].filter(a => a !== null);
        
        const cnt = csp.count(lines, 3);
        csp.assert(csp.or(csp.eq(cnt, n0), csp.eq(cnt, n2)));
      }
    }
    
    grid.iter((x, y, d) => {
      const lines = [
        hlines.get(x, y),
        vlines.get(x + 1, y),
        hlines.get(x, y + 1),
        vlines.get(x, y),
      ].filter(a => a !== null);
      
      const cnt = csp.count(lines, 3);
      csp.assert(csp.eq(cnt, d));
    });
    
    const cells = 1;
    
    if(cells){
      const itersNum = (w * h1 + w1 * h >> 1) + 2;
      let hlinesIter = new O.Grid(w, h1, (x, y) => [hlines.get(x, y), csp.var()]);
      let vlinesIter = new O.Grid(w1, h, (x, y) => [vlines.get(x, y), csp.b0]);
      let foundHline = csp.b0;
      
      hlines.iter((x, y, d) => {
        const fst = csp.and(csp.not(foundHline), d);
        foundHline = csp.or(foundHline, d);
        csp.assert(csp.eq(hlinesIter.get(x, y)[1], fst));
      });
      
      csp.assert(foundHline);
      
      for(let i = 0; i !== itersNum; i++){
        const hlines1 = new O.Grid(w, h1, (x, y) => {
          const [bit, act] = hlinesIter.get(x, y);
      
          const adjsAct = [
            hlinesIter.get(x - 1, y),
            hlinesIter.get(x + 1, y),
            vlinesIter.get(x, y - 1),
            vlinesIter.get(x, y),
            vlinesIter.get(x + 1, y - 1),
            vlinesIter.get(x + 1, y),
          ].filter(a => a !== null).map(a => a[1]);
      
          const bit1 = csp.and(bit, csp.not(act));
          const act1 = csp.and(bit1, csp.orv(adjsAct));
      
          return [bit1, act1];
        });
      
        const vlines1 = new O.Grid(w1, h, (x, y) => {
          const [bit, act] = vlinesIter.get(x, y);
      
          const adjsAct = [
            vlinesIter.get(x, y - 1),
            vlinesIter.get(x, y + 1),
            hlinesIter.get(x - 1, y),
            hlinesIter.get(x, y),
            hlinesIter.get(x - 1, y + 1),
            hlinesIter.get(x, y + 1),
          ].filter(a => a !== null).map(a => a[1]);
      
          const bit1 = csp.and(bit, csp.not(act));
          const act1 = csp.and(bit1, csp.orv(adjsAct));
      
          return [bit1, act1];
        });
      
        hlinesIter = hlines1;
        vlinesIter = vlines1;
      }
      
      const iterBits = [];
      
      hlinesIter.iter((x, y, [a, b]) => iterBits.push(a));
      vlinesIter.iter((x, y, [a, b]) => iterBits.push(a));
      
      csp.assert(csp.norv(iterBits));
    }else{
      // const m = (w * h1 + w1 * h >> 1) + 2;
      // const hlines1 = this.hlines1 = new O.Grid(w, h1, () => csp.varLt(m));
      // const vlines1 = this.vlines1 = new O.Grid(w1, h, () => csp.varLt(m));
      // 
      // let found = csp.b0;
      // 
      // const checkAdj = (k, d, [t, x, y]) => {
      //   const k1 = (t === 0 ? hlines : vlines).get(x, y);
      //   if(k1 === null) return null;
      // 
      //   const d1 = (t === 0 ? hlines1 : vlines1).get(x, y);
      // 
      //   csp.assert(csp.imp(csp.and(k, k1), csp.ge(d1, d)));
      // 
      //   return csp.and(k1, csp.eq(d1, d));
      // };
      // 
      // const checkAdjs = (k, d, adjs) => {
      //   const results = adjs.map(adj => checkAdj(k, d, adj)).filter(a => a !== null);
      //   csp.assert(csp.imp(k, csp.orv(results)));
      // };
      // 
      // hlines1.iter((x, y, d) => {
      //   const k = hlines.get(x, y);
      //   const nz = csp.orv(d);
      //   const z = csp.not(nz);
      //   const fst = csp.and(csp.not(found), k);
      // 
      //   // csp.assert(csp.eq(z, fst));
      //   csp.assert(csp.eq(z, csp.or(csp.not(k), fst)));
      // 
      //   checkAdjs(csp.and(k, nz), csp.dec(d), [
      //     [0, x - 1, y],
      //     [0, x + 1, y],
      //     [1, x, y - 1],
      //     [1, x, y],
      //     [1, x + 1, y - 1],
      //     [1, x + 1, y],
      //   ]);
      // 
      //   found = csp.or(found, k);
      // });
      // 
      // vlines1.iter((x, y, d) => {
      //   const k = vlines.get(x, y);
      // 
      //   csp.assert(csp.eq(csp.orv(d), k));
      // 
      //   checkAdjs(k, csp.dec(d), [
      //     [1, x, y - 1],
      //     [1, x, y + 1],
      //     [0, x - 1, y],
      //     [0, x, y],
      //     [0, x - 1, y + 1],
      //     [0, x, y + 1],
      //   ]);
      // });
      // 
      // csp.assert(found);
    }
    
    if(gen && !allow4){
      const n4 = csp.int(4, 3);
      const arr = [];
      
      grid.iter((x, y, d) => arr.push(csp.eq(d, n4)));
      csp.assert(csp.norv(arr));
    }
  }
  
  aels(){
    const {csp, w, h, hlines, vlines} = this;
    const wh = w / 2;
    const hh = h / 2;
    
    this.ael('mousedown', evt => {
      const {button, clientX, clientY} = evt;
      const {iw, ih, iwh, ihh} = O;
    
      if(button === 0 || button === 2){
        const xf = (clientX - iwh) / tileSize + wh;
        const yf = (clientY - ihh) / tileSize + hh;
        
        let x = floor(xf);
        let y = floor(yf);
        let dx = xf - x;
        let dy = yf - y;
        
        let lines = null;
        
        if(min(dx, 1 - dx) < min(dy, 1 - dy)){
          lines = vlines;
          if(dx > .5) x++;
        }else{
          lines = hlines;
          if(dy > .5) y++;
        }
        
        const k = lines.get(x, y);
        if(k === null || csp.isGivenBit(k)) return;
        
        const bit = csp.resolveBit(k);
        const bit1 = button === 0 ? 1 : 0;
        
        csp.assignBit(k, bit === null ? bit1 : null);
    
        return 1;
      }
    });
  }
  
  render1(){
    const {csp, g, w, h, grid, hlines, vlines} = this;
    const {iw, ih, iwh, ihh} = O;
    const w1 = w + 1;
    const h1 = h + 1;
    
    // csp.revealHidden();
    
    g.font(32);
    g.lineCap = 'square';
    g.lineJoin = 'miter';
    
    g.fillStyle = 'darkgray';
    g.fillRect(0, 0, iw, ih);
    
    g.translate(iwh, ihh);
    g.scale(tileSize);
    g.translate(-w / 2, -h / 2);
    
    g.fillStyle = '#e6e6e6';
    g.fillRect(0, 0, w, h);
    
    grid.iter((x, y, d) => {
      const v = csp.resolve(d);
      if(v === null) return;
    
      g.save();
      g.translate(x, y);
      g.fillStyle = 'black';
      g.fillText(v, .5, .5);
      g.restore();
    });
    
    g.strokeStyle = '#cfcfcf';
    g.beginPath();
    for(let y = 0; y <= h; y++){
      g.moveTo(0, y);
      g.lineTo(w, y);
    }
    for(let x = 0; x <= w; x++){
      g.moveTo(x, 0);
      g.lineTo(x, h);
    }
    g.stroke();
    
    const drawLines = (val, col) => {
      g.strokeStyle = col;
      
      [hlines, vlines].forEach((lines, i) => {
        const dx = i === 0 ? 1 : 0;
        const dy = i === 1 ? 1 : 0;
        
        lines.iter((x, y, d) => {
          const v = csp.resolveBit(d);
          if(v !== val) return;
          
          g.beginPath();
          g.moveTo(x, y);
          g.lineTo(x + dx, y + dy);
          g.stroke();
        });
      });
    };
    
    g.capMode = 1;
    g.lineWidth = 3;
    
    drawLines(null, '#cfcf00');
    drawLines(1, 'black');
    
    g.lineWidth = 1;
    g.capMode = 0;
    
    // if(this.hlines1){
    //   g.font(12);
    //   g.fillStyle = 'black';
    //   g.textBaseline = 'bottom';
    //   g.textAlign = 'center';
    //   this.hlines1.iter((x, y, d) => {
    //     const n = csp.resolve(d);
    //     g.fillText(n, x + .5, y);
    //   });
    //   g.textBaseline = 'middle';
    //   g.textAlign = 'right';
    //   this.vlines1.iter((x, y, d) => {
    //     const n = csp.resolve(d);
    //     g.fillText(n, x, y + .5);
    //   });
    //   g.textBaseline = 'middle';
    //   g.textAlign = 'center';
    // }
    
    g.resetTransform();
  }
}

module.exports = Slitherlink;