'use strict';

const fs = require('fs');
const path = require('path');
const electron = require('electron');
const assert = require('../../assert');
const O = require('../../js-util');
const LS = require('../../local-strings');
const Game = require('../game');
const InputInt = require('../managers/input-int');
const util = require('../util');

const {min, max, floor} = Math;

const gameId = 'sudoku';
const lsGame = LS(`games.${gameId}`);

const tileSize = 50;

class Sudoku extends Game{
  static getId(){ return gameId; }
  static getName(){ return `${lsGame('name')}`; }
  static getParamDescs(){ return LS.strs.games[gameId].params; }
  static getDefaultParams(){ return ['2', '2']; }
  
  setParams1(ps){
    if(ps.some(a => !util.isInt(a, 1, 100))) return 0
    
    const ws = this.ws = Number(ps[0])
    const hs = this.hs = Number(ps[1])
    const n = this.n = ws * hs
    
    return 1
  }
  
  getParamsStr(){
    return `${this.ws}x${this.hs}`;
  }
  
  aels(){
    this.mng.aels();
  }
  
  undoOrRedo1(){
    this.mng.undoOrRedo();
  }
  
  render1(){
    const {g, n, ws, hs, grid, mng} = this;
    const {iw, ih, iwh, ihh} = O;
    const nh = n / 2;
    const w = n;
    const h = n;
    const wh = nh;
    const hh = nh;
    
    g.font(32);
    g.lineCap = 'square';
    g.lineJoin = 'miter';
    
    g.fillStyle = 'darkgray';
    g.fillRect(0, 0, iw, ih);
    
    // if(1){
    //   g.textBaseline = 'top';
    //   g.textAlign = 'left';
    // 
    //   g.fillStyle = 'black';
    //   g.fillText(csp.missingCnt, 5, 5);
    //   g.fillText(csp.wrongCnt, 5, 5 + 32);
    // 
    //   g.textBaseline = 'middle';
    //   g.textAlign = 'center';
    // }
    
    g.translate(iwh, ihh);
    g.scale(tileSize);
    g.translate(-wh, -hh);
    
    g.fillStyle = 'white';
    g.fillRect(0, 0, w, h);
    
    this.mng.render();
    
    for(let y = 0; y !== h; y++){
      for(let x = 0; x !== w; x++){
        const v = this.get(x, y);
        if(v === null) continue;
        
        g.save();
        g.translate(x, y);
        g.fillStyle = 'black';
        g.fillText(v, .5, .5);
        g.restore();
      }
    }
    
    g.beginPath();
    for(let y = 0; y <= h; y++){
      g.moveTo(0, y);
      g.lineTo(w, y);
    }
    for(let x = 0; x <= w; x++){
      g.moveTo(x, 0);
      g.lineTo(x, h);
    }
    g.stroke();
    
    g.capMode = 1;
    g.lineWidth = 5;
    g.beginPath();
    for(let y = 0; y <= h; y += hs){
      g.moveTo(0, y);
      g.lineTo(w, y);
    }
    for(let x = 0; x <= w; x += ws){
      g.moveTo(x, 0);
      g.lineTo(x, h);
    }
    g.stroke();
    g.lineWidth = 1;
    g.capMode = 0;
    
    g.resetTransform();
  }
  
  get(x, y){
    const tile = this.grid.get(x, y);
    if(tile === null) return null;
    
    const v = this.resolve(tile);
    if(v === null) return null;
    
    return v + 1;
  }
  
  set(x, y, v){
    if(v !== null) assert(v > 0 && v <= this.n);
    
    const tile = this.grid.get(x, y);
    this.csp.assign(tile, v !== null ? v - 1 : null);
  }

  after_gen(){
    const {ws, hs, n} = this
    const w = n
    const h = n

    let i = 0;
    const grid = new O.Grid(w, h, () => i++)

    this.grid = grid
    this.mng = new InputInt(this, grid, tileSize, 1, 1, n)

    this.aels();
  }
}

module.exports = Sudoku;