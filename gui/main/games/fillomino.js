'use strict';

const fs = require('fs');
const path = require('path');
const electron = require('electron');
const assert = require('../../assert');
const O = require('../../js-util');
const LS = require('../../local-strings');
const Game = require('../game');
const InputInt = require('../managers/input-int');
const util = require('../util');

const {min, max, floor} = Math;

const gameId = 'fillomino';
const lsGame = LS(`games.${gameId}`);

const tileSize = 50;

class Fillomino extends Game{
  static getId(){ return gameId; }
  static getName(){ return lsGame('name'); }
  static getParamDescs(){ return LS.strs.games[gameId].params; }
  static getDefaultParams(){ return ['7', '7', '3']; }

  setParams1(ps){
    if(ps.some(a => !util.isInt(a, 1, 100))) return 0;

    const [w, h, ub] = ps.map(a => Number(a));
    const area = w * h;
    
    if(ub > w * h) return 0;
    
    if(ub < 3){
      if(w !== 1 && h !== 1) return 0;
      if(ub === 1 && area !== 1) return 0;
    }
    
    this.w = w;
    this.h = h;
    this.ub = ub;
    
    return 1;
  }
  
  getParamsStr(){
    return `${this.w}x${this.h} (${this.ub})`;
  }
  
  aels(){
    this.mng.aels();
  }
  
  undoOrRedo1(){
    this.mng.undoOrRedo();
  }
  
  render1(){
    const {g, w, h, grid, hlines, vlines, mng} = this;
    const {iw, ih, iwh, ihh} = O;
    const w1 = w + 1;
    const h1 = h + 1;
    
    g.font(32);
    g.lineCap = 'square';
    g.lineJoin = 'miter';
    
    g.fillStyle = 'darkgray';
    g.fillRect(0, 0, iw, ih);
    
    g.translate(iwh, ihh);
    g.scale(tileSize);
    g.translate(-w / 2, -h / 2);
    
    const checked = new Set();
    const finished = new Set();
    const wrong = new Set();
    
    grid.iter((x, y, d) => {
      if(checked.has(d)) return;
      checked.add(d);
      
      const v = this.resolve(d);
      if(v === null) return;
      
      const visited = new Set([d]);
      
      grid.iterAdj(x, y, (x1, y1, d1) => {
        if(checked.has(d1)) return 0;
        
        if(this.resolve(d1) === v){
          checked.add(d1);
          visited.add(d1);
          return 1;
        }
        
        return 0;
      });
      
      const num = visited.size;
      if(num < v) return;
      
      for(const d of visited)
        finished.add(d);
      
      if(num === v) return;
      
      for(const d of visited)
        wrong.add(d);
    });
    
    grid.iter((x, y, d) => {
      g.fillStyle = finished.has(d) ? wrong.has(d) ? '#f88' : '#8f8' : 'white';
      g.fillRect(x, y, 1, 1);
    });
    
    this.mng.render();
    
    g.beginPath();
    for(let y = 1; y !== h; y++){
      g.moveTo(0, y);
      g.lineTo(w, y);
    }
    for(let x = 1; x !== w; x++){
      g.moveTo(x, 0);
      g.lineTo(x, h);
    }
    g.stroke();
    
    g.capMode = 1;
    g.lineWidth = 3;
    g.beginPath();
    g.rect(0, 0, w, h);
    
    grid.iter((x, y, d) => {
      const v = this.resolve(d);
      const fin = finished.has(d);
      const nnull = v !== null;
      
      if(x !== 0){
        const d1 = grid.get(x - 1, y);
        const v1 = this.resolve(d1);
        const fin1 = finished.has(d1);
        
        if(v1 !== v && (fin || fin1 || (nnull && v1 !== null))){
          g.moveTo(x, y);
          g.lineTo(x, y + 1);
        }
      }
      
      if(y !== 0){
        const d1 = grid.get(x, y - 1);
        const v1 = this.resolve(d1);
        const fin1 = finished.has(d1);
        
        if(v1 !== v && (fin || fin1 || (nnull && v1 !== null))){
          g.moveTo(x, y);
          g.lineTo(x + 1, y);
        }
      }
      
      if(!nnull) return;
  
      g.save();
      g.translate(x, y);
      g.fillStyle = 'black';
      g.fillText(v, .5, .5);
      g.restore();
    });
    
    g.stroke();
    g.lineWidth = 1;
    g.capMode = 0;
    
    g.resetTransform();
  }

  after_gen(){
    const {w, h, ub} = this

    let i = 0;
    const grid = new O.Grid(w, h, () => i++)

    this.grid = grid
    this.mng = new InputInt(this, grid, tileSize, 0, 1, ub)

    this.aels();
  }
}

module.exports = Fillomino;