'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const electron = require('electron');
const O = require('../js-util');

const {min, max, ceil, log2} = Math;

const isInt = (a, lb, ub) => {
  if(!/^(?:0|\-?[1-9][0-9]*)$/.test(a)) return 0;
  
  const n = Number(a);
  return n >= lb && n <= ub;
};

const parseProp = a => {
  if(a === 'true') return 1;
  if(a === 'false') return 0;
  return null;
};

const bitsNum = n => {
  return max(ceil(log2(n)), 1);
};

module.exports = {
  isInt,
  parseProp,
  bitsNum,
};